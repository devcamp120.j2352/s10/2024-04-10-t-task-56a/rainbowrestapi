package devcamp.com.resapi.rainbowrestapi.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@RestController
@CrossOrigin
@RequestMapping("/api")
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowList() {
        ArrayList<String> rainbowList = new ArrayList<>();
        String[] arrRainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

        for (String rainbow : arrRainbows) {
            rainbowList.add(rainbow);
        }
        
        //rainbowList = new ArrayList<>(Arrays.asList(arrRainbows));
        return rainbowList;
    }
    
}
