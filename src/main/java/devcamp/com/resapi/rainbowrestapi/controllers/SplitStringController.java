package devcamp.com.resapi.rainbowrestapi.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;



@RestController
@CrossOrigin
@RequestMapping("/api")
public class SplitStringController {
    @GetMapping("/split")
    public ArrayList<String> getListWorld(@RequestParam(value="string", defaultValue = "mot hai ba bon") String input) {
        String[] arrWorld = input.split(" ");
        ArrayList<String> listWorlds = new ArrayList<>(Arrays.asList(arrWorld));

        return listWorlds;
    }
    
}
